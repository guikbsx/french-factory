//
//  Utils.swift
//  French Factory
//
//  Created by Guillaume Bisiaux on 27/11/2019.
//  Copyright © 2019 guik development. All rights reserved.
//

import Foundation

enum Action {
    case attack, heal
}

class Utils {
    
    static var names = [String]()
    static var round = Int()
    static var winner = String()
    
    /// Verify if your name is unique and return a boolean
    static func isUnique(name: String) -> Bool {
        if Utils.names.contains(name.lowercased()){
            return false
        }
        return true
    }
    
    /// Choose a name to your character
    static func chooseName() -> String {
        if let name = readLine() {
            if Utils.isUnique(name: name) {
                return name
            } else {
                print("This name is already taken.")
                sleep(1) // <- Faire patienter l'utilisateur pendant une seconde
                print("Please choose an other name :")
                return chooseName()
            }
        }
        print("An error occured. Please choose a unique name.")
        sleep(1) // <- Faire patienter l'utilisateur pendant une seconde
        return chooseName()
    }
    
    /// Add the current name as lowercased to names array
    static func add(name: String) {
        Utils.names.append(name.lowercased())
    }
    
    /// Display all type of character
    static func printAllCharacter() {
        let chars : [Character] =   [   Character(name: "DefaultAlchemist", type: .alchemist),
                                        Character(name: "DefaultArcher", type: .archer),
                                        Character(name: "DefaultBerserker", type: .berserker),
                                        Character(name: "DefaultAssassin", type: .assassin)     ]
        Utils.introduceCharacter(team: chars)
    }
    
    ///Print a  team
    static func printTeam(team: [Character]) {
        var counter = 1
        for char in team {
            if char.isAlive {
                print("\(counter). \(char.name)  | \(char.type) with \(char.weapon.name)")
                print("   💣: \(char.weapon.damage) \t ❤️: \(char.hp) \t 🌀: \(char.magic)")
            }
            counter += 1
        }
    }
    
    static func introduceCharacter(team: [Character]) {
        var counter = 1
        for char in team {
            print(  "\(counter). \(char.type) | \(char.weapon.name)")
            print("\t Damage 💣: \(char.weapon.damage)pts \t HP ❤️: \(char.hp)pts \t Magic 🌀: \(char.magic)pts ")
            counter += 1
        }
    }
        
}
