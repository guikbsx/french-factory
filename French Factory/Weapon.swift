//
//  Weapon.swift
//  French Factory
//
//  Created by Guillaume Bisiaux on 24/10/2019.
//  Copyright © 2019 guik development. All rights reserved.
//

class Weapon {
    let name: String
    let damage: Int
    
    init(name: String, damage: Int){
        self.name = name
        self.damage = damage
    }
}
