import Foundation

class Game {
    
    var players : [Player] = []
    
    init(){
        initParty()
        launchParty()
        finishParty()
    }
    
    func initParty() {
        
        print("Let's create teams.")
        sleep(1)
                
        for i in 1...2 {
            print("Player \(i), what's your name ? :")
            let name = Utils.chooseName()
            let player = Player(name: name, team: [])
            player.chooseCharacter()
            players.append(player)
        }
        
    }
    
    /// Launch the party !
    func launchParty() {
        
        var actingPlayer = players[0]
        var defendingPlayer = players[1]
        
        while actingPlayer.isAlive {
            print("\(actingPlayer.name), it's your turn.")
            let charToAct = actingPlayer.selectCharacter()
            charToAct.findASafe()
            let action = actingPlayer.chooseAction()
            switch action {
            case .attack:
                print("Choose an enemy to attack :")
                let defendingChar = defendingPlayer.selectCharacter()
                charToAct.attack(target: defendingChar)
                if !defendingChar.isAlive {
                    print("\(defendingChar.name) is KO...")
                    sleep(1)
                }
            case .heal:
                print("Choose a ally to heal :")
                let healedChar = actingPlayer.selectCharacter()
                charToAct.heal(target: healedChar)
            }
            Utils.round += 1
            Utils.winner = actingPlayer.name
            actingPlayer = actingPlayer === players[0] ? players[1] : players[0]
            defendingPlayer = actingPlayer === players[0] ? players[1] : players[0]
        }
        
    }
    
    func finishParty() {
        print("Party is over !")
        sleep(1)
        print("After \(Utils.round) rounds, the winner is \(Utils.winner).")
        sleep(1)
    }

}
