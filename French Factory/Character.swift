//
//  Character.swift
//  French Factory
//
//  Created by Guillaume Bisiaux on 24/10/2019.
//  Copyright © 2019 guik development. All rights reserved.
//
import Foundation

class Character {
    enum TypeOf: CaseIterable {
        case alchemist, archer, berserker, assassin
    }
    let name: String
    let type : TypeOf
    var hp: Int
    let magic: Int
    var weapon: Weapon
    var isAlive: Bool {
        if hp > 0 {
            return true
        }
        return false
    }
    
    init(name: String, type: TypeOf){
        self.name = name
        self.type = type
        switch type {
            case .alchemist:
                self.weapon = Weapon(name: "magic Wand 🥢", damage: 50)
                self.hp = 50
                self.magic = 50
            case .archer:
                self.weapon = Weapon(name: "ark 🏹", damage: 50)
                self.hp = 50
                self.magic = 25
            case .berserker:
                self.weapon = Weapon(name: "super fist 👊", damage: 50)
                self.hp = 50
                self.magic = 0
            case .assassin:
                self.weapon = Weapon(name: "knife 🗡", damage: 50)
                self.hp = 50
                self.magic = 20
        }
        Utils.add(name: name)
    }
    
    func attack(target: Character) {
        target.hp -= weapon.damage
        print("\(target.name) lost \(weapon.damage) hp.")
        sleep(2)
    }
    
    func heal(target: Character) {
        hp += target.magic
        print("\(target.name) regained \(magic) hp.")
        sleep(2)
    }
    
    func findASafe() {
        let randomInt = Int.random(in: 0...100)
        if randomInt < 10 {
            let new = Safe()
            print("‼️")
            sleep(1)
            print("You're opening a safe with a \(new.weapon.name).")
            weapon = new.weapon
            sleep(1)
        }
        
    }
    
    
    
    
    
}

