import Foundation

class Player {
    let name: String
    var team: [Character]
    var isAlive : Bool {
        for char in team {
            if char.isAlive {
                return true
            }
        }
        return false
    }
    
    init(name: String, team: [Character]){
        self.name = name
        self.team = team
        Utils.add(name: name)
    }
    
    func chooseCharacter(){
        while team.count < 3 {
            Utils.printAllCharacter()
            sleep(1)
            print("Choose the number of your character n°\(team.count + 1):")
            let type = chooseType()
            print("Choose a name to your \(type):")
            let name = Utils.chooseName()
            team.append(Character(name: name, type: type))
        }
    }
    
    private func chooseType() -> Character.TypeOf {
        var type : Character.TypeOf
        if let data = readLine() {
            switch data {
                case "1" : type = .alchemist
                case "2" : type = .archer
                case "3" : type = .assassin
                case "4" : type = .berserker
                default :
                    print("Please choose between 1 and 4... :")
                    return chooseType()
            }
        } else {
            return chooseType()
        }
        return type
    }
    
    func selectCharacter()-> Character {
        Utils.printTeam(team: team)
        sleep(1)
        print("Choose a character : ")
        if let data = readLine() {
            guard let intData = Int(data) else { return selectCharacter() }
            if intData > 0 && intData <= team.count {
                return team[intData-1]
            }
        } else {
            return selectCharacter()
        }
        return selectCharacter()
    }
    
    func chooseAction() -> Action {
        print("Choose a action.")
        sleep(1)
        print("1. Attack ?")
        print("2. Heal ?")
        if let data = readLine() {
            switch data {
            case "1" :
                return .attack
            case "2" :
                return .heal
            default:
                print("Please choose between 1 and 2... :")
                return chooseAction()
            }
        } else {
            return chooseAction()
        }
    }
    
}

