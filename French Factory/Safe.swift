//
//  Safe.swift
//  French Factory
//
//  Created by Guillaume Bisiaux on 22/01/2020.
//  Copyright © 2020 guik development. All rights reserved.
//

import Foundation

class Safe {
    let weapon: Weapon
    
    init() {
        let random = Int.random(in: 0...7)
        switch random {
        case 1: self.weapon = Weapon(name: "ice bomb", damage: 30)
        case 2: self.weapon = Weapon(name: "épée", damage: 40)
        case 3: self.weapon = Weapon(name: "shadow ball", damage: 50)
        case 4: self.weapon = Weapon(name: "boomerang", damage: 80)
        case 5: self.weapon = Weapon(name: "bombe", damage: 50)
        case 6: self.weapon = Weapon(name: "sniper", damage: 30)
        case 7: self.weapon = Weapon(name: "fire hand", damage: 150)
        default: self.weapon = Weapon(name: "default weapon", damage: 1)
        }
    }
    
}
